from flask import Flask,render_template
from flask_pymongo import PyMongo
from flask_socketio import SocketIO,emit,send
import json
from datetime import datetime

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysecret'
socketio = SocketIO(app)

app.config['MONGO_DBNAME']='PPAP_OEE'
app.config["MONGO_URI"] = "mongodb://localhost:27017/PPAP_OEE"
mongo = PyMongo(app)

mycollection=mongo.db.production_orders #Here myTable is your Table Name
mycollection1=mongo.db.shift_records
cursor1=mycollection.find()
print(type(cursor1))
'''
for rec in cursor1:

    print("++++++",type(rec))
'''

@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)
'''@socketio.on('message')
def handleMessage(msg):
	print('Message: ' + msg)
	send(msg, broadcast=True)'''


@socketio.on('production_order')
def get_production_order():
    print("order recieved")
    myquery={'MACHINE': 'JSW 650T'}
    records = mycollection.find(myquery, {'_id': False})
    ret_order=[]
    for rec in records:
        rec['PLANNED_STOP'] = rec['PLANNED_STOP'].strftime('%y.%m.%d %H:%M:%S')
        rec['PLANNED_START'] = rec['PLANNED_START'].strftime('%y.%m.%d %H:%M:%S')
       #rec['ACTUAL_START'] = rec['ACTUAL_START'].strftime('%y.%m.%d %H:%M:%S')
       # rec['ACTUAL_STOP'] = rec['ACTUAL_STOP'].strftime('%y.%m.%d %H:%M:%S')
        ret_order.append(rec)
    print(type(ret_order))
   #data=[{'name':'xyz'},{'name':'lmn'}]
    emit("production_order_response", ret_order)



@app.route('/order')
def success():#return "mongodb... "
    try:
        myquery={'MACHINE': 'JSW 650T'}
        records=mycollection.find(myquery,{'_id':False})
        ret_order=[]
        for rec in records:
            ret_order.append(rec)
        for rec in ret_order:
           rec['PLANNED_STOP'] = rec['PLANNED_STOP'].strftime('%y.%m.%d %H:%M:%S')
           rec['PLANNED_START']= rec['PLANNED_START'].strftime('%y.%m.%d %H:%M:%S')


        return render_template('index.html',records=ret_order,)
    except Exception as e:
        return json.dumps({'error':str(e)})
'''
@app.route('/shift_order')
def success_shift_order():#return "mongodb... "
    try:
        records=mycollection1.find({'$and': [{'SHIFT DATE':"03/09/2020"},{'SHIFT DATE':"04/09/2020"}]})
        return render_template('index.html',records=records)
    except Exception as e:
        return json.dumps({'error':str(e)})
'''



if __name__ == '__main__':
   socketio.run(app,debug=True)